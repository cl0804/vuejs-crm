<?php
header( 'Content-Type: application/json' );
header( 'Access-Control-Allow-Origin: *' );

require_once '../../init.php';
require_once '../../apiTypes.php';

  $resp = new ApiResponse();

// Autoload Composer.
require_once 'A:\PHP Stuff\Xamp\htdocs\arc-crm\backend\vendor\autoload.php';

$userCreds = __DIR__ . '/creds.json';
$client = getClient($userCreds);
$service = new Google_Service_Sheets($client);

$spreadsheetId = '1WgcQLmnqqqCJqqpNGe-GmmUjO7jRCt_rw-TaaYMS0NY';  
$range = 'Form responses 1'; 

$rows = $service->spreadsheets_values->get($spreadsheetId, $range, ['majorDimension' => 'ROWS']);

$lineOneValues = [
    [
        'Flag',
        'Total Amount Paid',
        'Payment Type',
        'Register Timestamp',
        'Notes',
    ],
    // Additional rows ...
];
$lineOneData = [];
$lineOneData[] = new Google_Service_Sheets_ValueRange([
    'range' => END_OF_FORM_DATA.'1:'.END_OF_APP_DATA.'1',
    'values' => $lineOneValues
]);

$requestBody = new Google_Service_Sheets_BatchUpdateValuesRequest([
  'valueInputOption' => 'USER_ENTERED',
  'data' => $lineOneData
]);

$lineOneResponse = $service->spreadsheets_values->batchUpdate($spreadsheetId, $requestBody);


//Set new column values for use in app
if (isset($rows['values'])) {
  // var_dump($rows['values']);
  $updateRange = array();
  $sheetRow = 2;
  $index = 1;
  $updateData = [];
  foreach ($rows['values'] as $row) {

    //Only do it for rows that exist
    if( empty($rows['values'][$index][24]) && !empty($rows['values'][$index][1]) ) {

      $updateValues = [
          [
              'FALSE',
              '0',
              'null',
              'null',
              'null',
          ]
      ];
      
      $updateData[] = new Google_Service_Sheets_ValueRange([
        'range' => END_OF_FORM_DATA.$sheetRow.':'.END_OF_APP_DATA.$sheetRow,
        'values' => $updateValues      
      ]);
    }
        

    $index++;
    $sheetRow++;
  }

  $requestBody = new Google_Service_Sheets_BatchUpdateValuesRequest([
        'valueInputOption' => 'USER_ENTERED',
        'data' => $updateData
      ]);

      $lineOneResponse = $service->spreadsheets_values->batchUpdate($spreadsheetId, $requestBody);
}


$response = $service->spreadsheets_values->get($spreadsheetId, $range);
$values = $response->getValues();

if( $values ) {
  $length = count($values);

  $newVals = array();

  // echo $length;
  // echo count($dataKeys);

  var_dump($values);

  for( $i = 0; $i < $length; $i++) {
   $newVals[] = array_combine( $dataKeys, $values[$i]);
  }
  var_dump($newVals);

  //Take off the top array as it's just the headers
  array_splice($newVals, 0 ,1);

  //Create IDs for the rows as data is coming from Google Sheets not a nice DB
  for( $i = 0; $i - count($newVals); $i++) {
    array_unshift($newVals[$i], $i+1);
  }
  $resp->data['values'] = $newVals;
  $resp->message = 'Data received';
  echo json_encode($resp);
  http_response_code(200);
} else {
  $resp->message = 'Sad face emoji';
  echo json_encode($resp);
  http_response_code(404);
}


function getClient($userCreds) {
  $client = new Google_Client();
  $client->setApplicationName('Google Sheets and PHP');
  $client->setScopes(Google_Service_Sheets::SPREADSHEETS);
  $client->setAuthConfig( $userCreds );
  $client->setAccessType('offline');

  // $tokenPath = __DIR__ . '/token.json';
  // if (file_exists($tokenPath)) {
  //  $accessToken = json_decode(file_get_contents($tokenPath), true);
  //  $client->setAccessToken($accessToken);
  // }

  // // If there is no previous token or it's expired.
  // if ($client->isAccessTokenExpired()) {
  //  // Refresh the token if possible, else fetch a new one.
  //  if ($client->getRefreshToken()) {
  //      $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
  //  } else {
  //      // Request authorization from the user.
  //      $authUrl = $client->createAuthUrl();
  //      printf("Open the following link in your browser:\n%s\n", $authUrl);
  //      print 'Enter verification code: ';
  //      $authCode = trim(fgets(STDIN));

  //      // Exchange authorization code for an access token.
  //      $accessToken = $client->fetchAccessTokenWithAuthCode($authCode);
  //      $client->setAccessToken($accessToken);

  //      // Check to see if there was an error.
  //      if (array_key_exists('error', $accessToken)) {
  //          throw new Exception(join(', ', $accessToken));
  //      }
  //  }
  //  // Save the token to a file.
  //  if (!file_exists(dirname($tokenPath))) {
  //      mkdir(dirname($tokenPath), 0700, true);
  //  }
  //  file_put_contents($tokenPath, json_encode($client->getAccessToken()));
  // }
  return $client;
}


?>