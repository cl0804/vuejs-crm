import App from '../components/app.vue';
// import { routeAuthGuard } from './function.js';
import Login from '../components/login.vue';
import Nav from '../components/nav.vue';
import Register from '../components/register.vue';
import Secret from '../components/secret.vue';
import Secret2 from '../components/secret2.vue';
import store from '../store';

function routeAuthGuard(to, from, next) {
	if( store.getters.isUserLoggedIn ) {
		next();
	} else {
		next('/');
	}
}

function LoggedInCheck( to, from, next) {
	if( to.matched.some(record => record.meta.requiresVisitor)) {
		console.log('Visitors only');
	} else {
		next();
	}
}

export default [
	{
		component: App,
		name: 'home',
		path: '/'
	},
	{
		
		component: Login, 
		meta: {
			requiresVisitor: true,
		},
		name: 'login',
		path: '/login'
	},
	{
		component: Register, 
		meta: {
			requiresVisitor: true,
		},
		name: 'register',
		path: '/register'
	},
	{
		beforeEnter: (to, from, next) => {
			routeAuthGuard(to, from, next);
		},
		component: Secret, 
		name: 'secret',
		path: '/secret', 
	},
	{
		beforeEnter: (to, from, next) => {
			routeAuthGuard(to, from, next);
		},
		component: Secret2, 
		name: 'secret2',
		path: '/secret2', 
	}
]