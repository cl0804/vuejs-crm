<?php

$host = "localhost";
$database = "arc-crm";
$user = 'root';
$password = 'password';

$dsn = 'mysql:dbname='.$database.';host='.$host;

try {
    $db = new PDO($dsn, $user, $password);
} catch (PDOException $e) {
    echo 'Connection failed: ' . $e->getMessage();
}
