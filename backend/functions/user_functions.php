<?php

function CheckUsernameExists( $Username, $MySqliConn )
{
  if( empty( $Username ) )
    return false;

  // This should do it ;)
  $Username = filter_var( $Username, FILTER_SANITIZE_STRING );
  $Username = filter_var( $Username,  FILTER_SANITIZE_SPECIAL_CHARS );
  $Username = preg_replace( "/[^a-zA-Z0-9_]+/", "", $Username );

  if( !$Statement = $MySqliConn->prepare( "SELECT id FROM users WHERE username = ?" ) )
  {
    // failed to prepare statement
    return false;
  }

  $Statement->execute([$username]);
  $Statement->store_result();

  if( !$Statement )
  {
    // user actually doesn't exist
    return false;
  }

  return true;
} 

function ValidatePassword( $Password )
{
  global $USER_ErrorString;

  if( empty( $Password ) )
  {
    $USER_ErrorString = "Password cannot be empty";
    return false;
  }

  $Password = filter_var( $Password, FILTER_SANITIZE_STRING );

  $PasswordLen = strlen( $Password );

  if( $PasswordLen < 3 ||
      $PasswordLen > 20 )
  {
    $USER_ErrorString = "Password must be between 3 and 20 characters";
    return false;
  }

  return true;
}

function CreateUser( $Username, $Password, $MySqliConn )
{

  // if( !ValidateUsername( $Username ) )
  // {
  //   return false;
  // }

  if( CheckUsernameExists( $Username, $MySqliConn ) )
  {
    return false;
  }

  if( !ValidatePassword( $Password ) )
  {
    return false;
  }

  $Username = filter_var( $Username, FILTER_SANITIZE_STRING );
  $Username = filter_var( $Username,  FILTER_SANITIZE_SPECIAL_CHARS );
  $Username = preg_replace( "/[^a-zA-Z0-9_]+/", "", $Username );

  $Password = filter_var( $Password, FILTER_SANITIZE_STRING );

  $HashedPassword = password_hash( $Password, PASSWORD_BCRYPT );

  if( !$Statement = $MySqliConn->prepare( "INSERT INTO users( user_name, password ) VALUES( ?, ? )" ) )
  {
    $USER_ErrorString = "Failed to prepare SQL statement: " . $MySqliConn->errno . " " . $MySqliConn->error;
    return false;
  }

  $Statement->bind_param( 'ss', $Username, $HashedPassword );

  if( !$Statement->execute() )
  {
    $USER_ErrorString = "Failed to prepare to create user";
    return false;
  }

  return true;
}