<?php

define( "END_OF_FORM_DATA",'Y' );
define( "END_OF_APP_DATA",'AC' );

$dataKeys = array(
	0 => 'timestamp',
	1 => 'main-contact',
	2 => 'main-contact-age',
	3 => 'main-contact-email',
	4 => 'second-name',
	5 => 'second-age',
	6 => 'third-name',
	7 => 'third-age',
	8 => 'fourth-name',
	9 => 'fourth-age',
	10 => 'fifth-name',
	11 => 'fifth-age',
	12 => 'sixth-name',
	13 => 'sixth-age',
	14 => 'accommodation',
	15 => 'days',
	16 => 'total-balance',
	17 => 'deposit',
	18 => 'deposit-type',
	19 => 'donation',
	20 => 'requirements',
	21 => 'vehicle-reg',
	22 => 'data-consent',
	23 => 'photo-consent',
	24 => 'flag',
	25 => 'total-paid',
	26 => 'payment-type',
	27 => 'register-stamp',
	28 => 'notes'
);

$dataKeys2 = array(
	0 => 'timestamp',
	1 => 'main-contact',
	3 => 'main-contact-email',
);

class responseType 
{
	public $timestamp;
	public $main_contact;
	public $main_contact_email;
}