// Colors

$dusty-gray: #999;
$gallery: #eeeeee;
$alto: #ddd;
$bittersweet: #ff6f69;
$buttermilk: rgba(255, 238, 173, 0.4);
$vista-blue: #42b983;
$pink: rgba(255, 182, 193, 0.3);
$pickled-bluewood: #35495e;
$text: rgb(25,25,25);
$highlight: #eef20c;

$calendar-border: 1px solid $alto;

@mixin calendar-row {
  display: flex;
  justify-content: flex-start;
  width: 100%;
}

@mixin calendar-cell {
  width: 100%;
  padding: 0.5rem;
}

@font-face {
  font-family: 'Muli';
  src: url('~./assets/Muli-Light.ttf') format('truetype');
  font-weight: normal;
  font-style: normal;
}

@font-face {
  font-family: 'Muli';
  src: url('~./assets/Muli-Regular.ttf') format('truetype');
  font-weight: bold;
  font-style: normal;
}

body {
  font-family: 'Muli', Helvetica, Arial, sans-serif;
  color: $pickled-bluewood;
  margin: 0;
  padding-bottom: 2rem;

  display: flex;
  justify-content: center;

  * {
    box-sizing: border-box;
  }


  // Test
  background-color: $gallery;

  p {
    margin: 0;
    margin-bottom: 1rem;
  }

  .grid {
     margin: 0 auto;
    max-width: 1170px;
    padding-left: 1rem;
    padding-right: 1rem;
    width: 100%;
  }

  .button,
  a.button {
    background: transparent;
    border: 2px solid $text;
    color: $text;
    display: inline-block;
    outline: none;
    padding: 0.8rem 2rem;
    transition: all 0.3s ease-in-out;

    &:focus {
      outline: none;
    }

    &:hover {
      background: $text;
      color: $gallery;
      cursor: pointer;
    }

    &-inverted {
      background: transparent;
      border: 2px solid $gallery;
      color: #fff;

      &:hover {
        background: $gallery;
        color: $text;
      }
    }
  }

  #login {
    background: rgba($text, 0.58);
    border: 2px solid $text;
    box-shadow: 8px 6px 15px rgba(0,0,0,0.15);
    color: #fff;
    display: flex;
    flex-direction: column;
    margin: 4rem auto 0;
    max-width: 600px;
    padding: 3rem;

    input[type='text'],
    textarea,
    input[type='password'],
    input[type='email'] {
      -webkit-appearance: none;
      -moz-appearance:    none;
      appearance:         none;
      background: #f9f9f9;
      border: none;
      box-sizing: border-box;
      font-size: 1em;
      height: 50px;
      margin-bottom: 1rem; 
      outline: none;
      padding: 0 20px;
      resize: none;
      width: 100%;

    }
  }

  #app {
   width: 100%;
  }

  .status {
    background-color: lighten($gallery, 0.2);
    border: 1px solid #ccc;
    box-shadow: 8px 6px 15px rgba(0,0,0,0.15);
    opacity: 0;
    padding: 2rem;
    position: absolute;
    right: 4rem;
    top: 4rem;
    transform: translateX(2rem);
    transition: all 0.3s ease, opacity 0.4s ease, z-index 0s linear 0.4s;
    z-index: -1;

    h2 {
      margin: 0;
      margin-bottom: 0.5rem;
    }

    &.active {
      display: block;
      opacity: 1;
      transform: none;
      transition: all 0.3s ease, opacity 0.4s ease, z-index 0s linear 0s;
      z-index: 2;
    }
  }

  .navbar {
    background: rgba(25, 25, 25, 0.8);
    padding: 1rem;
    width: 100%;

    .nav-container {
      display: flex;
      justify-content: flex-end;
    }

    #menu {
      align-items: center;
      color: #fff;
      display: flex;
      flex-direction: row;
      justify-content: flex-end;
      list-style-type: none;
      width:50%;

      li {
        margin-right: 1rem;
      }

      a {
        color: #fff;
        display: block;
        font-weight: bold;
        padding: 1rem;
        text-decoration: none;

        &:hover {
          color: $highlight;
        }
      }
    }
  }

  .search-area {
    align-items: center;
    border: 1px solid rgba(25,25,25,1);
    box-shadow: 0px 4px 10px rgba(0,0,0,0.2);
    display: flex;
    flex-direction: column;
    justify-content: center;
    margin-top: 2em;
    padding: 2em;

    .title {
      margin:0 0 1em;
    }

    input {
      height: 45px;
      min-width: 250px;
      padding: 1em;
    }
  }

  .table {
    border: 2px solid #000;
    display: flex;
    flex-direction: column;
    margin-top: 2rem;

    &-header {
      background: darken($gallery, 5%);
      border-bottom: 1px solid #000;
      display: flex;
      flex-direction: row;
      font-weight: bold;
      text-transform: uppercase;
    }

    &-row {
      background: transparent;
      display: flex;
      flex-direction: row;
      transition: all 0.3s ease;

      &:nth-of-type(2n) {
        background: darken($gallery, 2%);
      }

      &:hover {
        background: darken($gallery, 4%);
      }

      &.flagged {
        background: rgba(159, 90, 253, 0.1);
      }
    }

    &-cell {
      padding: 1rem;
      width: 25%;

      &.table-names {
        width: auto;
      }
    }
  }

  .loader {
    margin: 0 auto;
    max-height: 100px;
    max-width: 100px;
  }

  .modal-overlay {
    align-items: center;
    background: rgba(0,0,0,0.7);
    display: flex;
    height: 100%;
    left: 0;
    opacity: 0;
    position: absolute;
    top: 0;
    transform: translateX(-100%);
    transition: all 0.3s ease, transform 0s linear 0s;
    width: 100%;
    z-index: -1;

    &.active {
      opacity: 1;
      transform: none;
      z-index: 1;
    }
  }

  .modal {
    background: #fff;
    box-sizing: border-box;
    margin: 0 auto;
    max-width: 1024px;
    position: relative;
    width: 100%;

    &-header {
      background: $gallery;
      color: rgba(25,25,25,1);
      padding: 2rem;

      h2 {
        margin: 0;
        text-transform: uppercase;
      }

      &.flagged {
        background: rgba(159, 90, 253, 0.2);
      }
    }

    .close {
      position: absolute;
      right: 2rem;
      top: 2rem;
    }

    .lead-name {
      width: 100%;
    }

    .box-row {
      display: flex;
      flex-direction: row;
      flex-wrap: wrap;
      justify-content: space-between;
      padding: 2em 2em 0;

      .box-column {
        width: calc(50% - 2em);

        &.register-box {
          align-items: center;
          display: flex;
          flex-direction: column;
          justify-content: center;

          > div {
            align-items: center;
            display: flex;
            flex-direction: column;
            justify-content: center;
          }
        }
      }

      &:last-of-type {
        padding-bottom: 2em;
      }
    }

    .balance-sheet {
      background: $gallery;
      border: 1px solid rgb(25,25,25);
      box-shadow: 0px 4px 10px rgba(0,0,0,0.2);
      padding: 1em;
      position: relative;

      .column-title {
        border-bottom: 1px solid rgb(25,25,25);
        margin: 0;
        padding-bottom: 0.5em;
      }

      h4 {
        margin: 0;
      }

      .row {
        align-items: center;
        border-bottom: rgba(25,25,25, 0.6);
        display: flex;
        flex-direction: row;
        justify-content: space-between;

        .amount,
        .title {
          padding: 0.5em 0;
        }

        .title {
          font-weight: bold;

          p {
            font-size: 0.8em;
            font-weight: normal;  
          }
        }

        &-final {
          border-top: 2px solid rgba(25,25,25,0.8);
          font-weight: bold;
          margin-top: 1.5em;
        }
      }

      &:before {
        background: rgba(25,25,25,0.8) url('./assets/loading-circle.gif') center / 10% no-repeat;
        content: '';
        height: 100%;
        left: 0;
        opacity: 0;
        position: absolute;
        top: 0;
        transition: all 0s linear, opacity 0.2s ease;
        visibility: hidden;
        width: 100%;
        z-index: -1;
      }

      &.loading {
        &:before {
          opacity: 1;
          visibility: visible;
          z-index: 2;
        }
      }
    }

    .table {
      border: 1px solid rgba(25,25,25,1);
      box-shadow: 0px 4px 10px rgba(0,0,0,0.2);
      margin-top: 0;

      &-header {
        background: $gallery;
        padding: 1em;
        text-transform: capitalize;

        h2 {
          margin: 0;
        }
      }
    }

    #update-balance {
      max-width: unset;
      width: 100%;

      label,
      .payment-type-container {
        align-items: center;
        display: flex;
        flex-direction: row;
        justify-content: space-between;
      }

      .payment-type-container select {
        padding: 0.5em;
      }

      .amount {
        .sum-box {
          border: 1px solid rgba(25,25,25,0.8);
          display: flex;

          .icon {
            align-items: center;
            background: rgba(25,25,25,0.8);
            color: #fff;
            display: flex;
            font-weight: bold;
            justify-content: center;
            padding: 0.5em 0.75em;
          }
        }

        input[type=number]::-webkit-inner-spin-button, 
        input[type=number]::-webkit-outer-spin-button { 
          -webkit-appearance: none; 
          margin: 0; 
        }

        input {
          -webkit-appearance: none;
          -moz-appearance: none;
          appearance: none;
          border: none;
          height: 40px;
          line-height: 50px;
          max-width: 85px;
          padding: 0 0.8em;
          width: 100%;
        }
      }

      .button {
        margin-top: 1em;
        width: 100%;
      }
    }

    .balance {
      width: 100%;
    }

    #notes,
    .notes {
      width: 100%;

      textarea {
        color: rgba(25,25,25,1);
        font-style: normal;
        height: 100px;
        overflow-y: scroll;
        padding: 1em;
        resize: none;
        width: 100%;
      }
    }

    .notes {
      background: $gallery;
      border: 1px solid rgba(25,25,25,1);
      box-shadow: 0px 4px 10px rgba(0,0,0,0.2);
      padding: 1em 1em 2em;
      position: relative;

      .column-title {
        border-bottom: 1px solid rgba(25,25,25,1);
        margin: 0;
        padding-bottom: 0.5em;
      }

      &:before {
        background: rgba(25,25,25,0.8) url('./assets/loading-circle.gif') center / 10% no-repeat;
        content: '';
        height: 100%;
        left: 0;
        opacity: 0;
        position: absolute;
        top: 0;
        transition: all 0s linear, opacity 0.2s ease;
        visibility: hidden;
        width: 100%;
        z-index: -1;
      }

      &.loading {
        &:before {
          opacity: 1;
          visibility: visible;
          z-index: 2;
        }
      }
    }

    #notes {
      display: flex;
      flex-direction: column;
      margin-top: 1em;

      .button {
        margin-top: 1em;
      }
    }
  }

}
