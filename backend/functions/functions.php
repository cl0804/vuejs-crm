<?php

// $ErrorString[] = 'No Error';

function StartSession()
{
  $SessionName = "session_id"; // Set session name
  $Secure = false; // becasue not using HTTPS (yet) set false for now
  $HttpOnly =  true; // Stops JavaScript accesing the session Id

  if( ini_set( 'session.use_only_cookies', 1 ) === false )
  {
    //header(../web_pages/error_pages/404.php); TODO need an error page to redirect to
    echo( "Failed to initiate a secure session( ini_set )" );
    exit();
  }

  // Grab the cookies parameters
  // $CookieParams = session_get_cookie_params();

  // session_set_cookie_params( $CookieParams["lifetime"],
  //                            $CookieParams["path"],
  //                            $CookieParams["domain"],
  //                            $Secure,
  //                            $HttpOnly );

  // Set the session name to the one set above
  session_name( $SessionName );
  session_start();  // Start the PHP session
  session_regenerate_id();  // regenerate the session and delete the old one

}

function LogOut($UserId, $MySqliConn) {
  global $ErrorString;

  if( !$stmt = $MySqliConn->prepare('DELETE FROM tokens WHERE user_id = ?') )
  {
    $ErrorString[] = 'Failed to prepare statement';
    return false;
  }

  $stmt->execute([$UserId]);

  if( !$stmt->execute([$UserId]) )
  {
    $ErrorString[] = "Failed to destroy tokens";
    return false;
  }

  return true;
}

function Login( $Username, $Password, $MySqliConn, $token) {
  global $ErrorString;

  // Going to being using prepared statements to reduce the risk of SQL injection
  if( !$stmt = $MySqliConn->prepare('SELECT * FROM users WHERE username = ?') )
  {
    // failed to prepare the statement
    $ErrorString[] = 'Failed to prepare statement';
    return false;
  }

  $Username = filter_var( $Username, FILTER_SANITIZE_STRING );
  $Username = filter_var( $Username,  FILTER_SANITIZE_SPECIAL_CHARS );
  $Username = preg_replace( "/[^a-zA-Z0-9_]+/", "", $Username );

  $Password = filter_var( $Password, FILTER_SANITIZE_STRING );

  $stmt->execute([$Username]);
  $user = $stmt->fetch(PDO::FETCH_ASSOC);


  //Hack to reset password
  // $HashedPassword = password_hash( $Password, PASSWORD_BCRYPT );
  // echo $HashedPassword;

  if( !$stmt->rowCount() )
  {
    // No user exists
    return false;
  }

  // User exists

  // Check password is typed in matches password in database
  // if this works then its some dark vodo magic i dont really
  // understand and that scares me
  // password_hash and password_verify do some crazy things
  if( !password_verify( $Password, $user['password'] ) )
  {
    $ErrorString[] = 'Password check failed';
    return false;
  }

  // Password is correct

  // XXS protection
  $UserId = preg_replace( "/[^0-9]+/", "", $user['id'] );
  $_SESSION["UserId"] = $UserId;

  // Check if already logged in
  if( checkToken($token, $UserId, $MySqliConn) ) {
    // echo nl2br( "Token already exists. User is currently logged in." );
    $ErrorString[] = 'Token already exists. User is currently logged in.';
    return false;
  }

  createToken($token, $UserId, $MySqliConn);

  return true;
}

function IsLoggedIn( $token, $MySqliConn ) {

  global $ErrorString;

  if( !$stmt = $MySqliConn->prepare('SELECT user_id FROM tokens WHERE access_token = ?') )
  {
    $ErrorString[] = 'Failed to prepare statement';
    return false;
  }

  if( !$stmt->execute([$token]) )
  {
    $ErrorString[] = 'Failed to execute statement';
    return false;
  }

  if( !$stmt->rowCount() )
  {
    // No user exists
    $ErrorString[] = 'Failed to find token';
    return false;
  }

  // $user = $stmt->fetch(PDO::FETCH_ASSOC);

  // print_r($user);

  return true;
}


function outputErrors($errors){
  $output = array();

  foreach($errors as $error){
      $output[] = '<li>' . $error . '</li>';
  }

  return '<ul>' . implode("", $output) . '</ul>';
}

function SanitiseInputStr( $String ) {
  global $ErrorString;

  if( '' == $String )
  {
    return $String;
  }

  $String = filter_var( $String, FILTER_SANITIZE_STRING );
  $String = filter_var( $String,  FILTER_SANITIZE_SPECIAL_CHARS );

  $String = preg_replace( '|[^a-z0-9-~+_.?#=!&;,/:%@$\|*\'()\\x80-\\xff]|i', '', $String );

  $Strip = array( '%0d', '%0a', '%0D', '%0A' );

  $String = (string)$String;

  $Count = 1;
  while( $Count )
  {
    $String = str_replace( $Strip, '', $String, $Count );
  }

  $String = htmlentities( $String );

  return $String;
}


function generate_string($input, $strength = 16) {
    $input_length = strlen($input);
    $random_string = '';
    for($i = 0; $i < $strength; $i++) {
        $random_character = $input[mt_rand(0, $input_length - 1)];
        $random_string .= $random_character;
    }
 
    return $random_string;
}

function createToken( $token, $UserId, $MySqliConn ) {
  global $ErrorString;

  if( !$Statement = $MySqliConn->prepare( "INSERT INTO tokens( access_token, user_id ) VALUES( ?, ? )" ) )
  {
    $ErrorString[] = "Failed to prepare SQL statement: " . $MySqliConn->errno . " " . $MySqliConn->error;
    return false;
  }

  $timestamp = time();

  // $Statement->bindParam( 'si', $token, $UserId );

  if( !$Statement->execute([$token, $UserId]) )
  {
    $ErrorString[] = 'Failed to create token';
    return false;
  }

  return true;
}

function checkToken( $token, $UserId, $MySqliConn) {
  global $ErrorString;

  if( !$stmt = $MySqliConn->prepare( "SELECT * FROM tokens WHERE user_id = ? AND access_token = ? LIMIT 1" ) )
  {
    // failed to prepare statement
    $ErrorString[] = 'Failed to prepare statement';
    return false;
  }

  $stmt->execute([$UserId, $token]);
  $user = $stmt->fetch(PDO::FETCH_ASSOC);


  if( !$stmt->rowCount() )
  {
    // No token exists
    $ErrorString[] = 'Failed to find token';
    return false;
  }

  return true;
}

function isUserAllowed( $UserId, $MySqliConn ) {
  global $ErrorString;
  if( !$stmt = $MySqliConn->prepare( "SELECT is_allowed FROM users WHERE id = ? " ) )
  {
    // failed to prepare statement
    $ErrorString[] = 'Failed to prepare statement';
    return false;
  }

  $stmt->execute([$UserId]);
  $user = $stmt->fetch(PDO::FETCH_ASSOC);


  if( !$stmt->rowCount() )
  {
    // No token exists
    $ErrorString[] = 'No token exists';
    return false;
  }

  return $user['is_allowed'];
}

function registerNewUser( $username, $password, $MySqliConn) {
  global $ErrorString;

  //Clean values up again just in case
  $username = SanitiseInputStr($username);
  $password = SanitiseInputStr($password);

  //hash dat pass
  $password = password_hash( $password, PASSWORD_BCRYPT );


 if( !checkForUser($username, $MySqliConn) ) {
  return false;
 }

  if( !$stmt = $MySqliConn->prepare( "INSERT INTO users( username, password, is_allowed ) VALUES( ?, ?, ? )" ) )
  {
    // failed to prepare statement
    return false;
  }

  if( !$stmt->execute([$username, $password, 0]) )
  {
    $ErrorString = "Failed to register user";
    return false;
  }

  return true;
}

function checkForUser( $username, $MySqliConn ) {
  global $ErrorString;

  if( empty($username) ) {
    return false;
  }

  $username = SanitiseInputStr($username);

  if( !$stmt = $MySqliConn->prepare( "SELECT id FROM users WHERE username = ? " ) )
  {
    // failed to prepare statement
    $ErrorString[] = 'Failed to prepare statement';
    return false;
  }

  if( !$stmt->execute([$username]) ) {
    $ErrorString[] = 'Failed to execute statement';
  }

  if( $stmt->rowCount() >= 1 )
  {
    $ErrorString[] = 'User already exists';
    return false;
  }

  return true;
}