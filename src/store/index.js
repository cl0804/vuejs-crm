import axios from 'axios';
import moment from 'moment-timezone';
import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);
moment.tz.setDefault('UTC');

let baseApiUrl = "http://localhost/arc-crm/backend/api/"

export default new Vuex.Store({
	state: {
		token: localStorage.getItem('access_token') || '',
	    user : 0,
	    status: '',
	    sheetsData: ''
	},
	mounted() {
	    if (localStorage.UID) {
	      state.token = localStorage.UID;
	    }
	},
	mutations: {
		statusChange(state, newState) {
			state.status = newState
		},
		retrieveToken( state, token) {
			state.token = token
		},
		destroyToken( state ) {
			state.token = null
		},
		setUser( state, value ) {
			state.user = value
		},
		setSheetData( state, data ) {
			state.sheetData = data
		}
	},
	actions: {
		retrieveToken(context, credentials){
			return new Promise((resolve, reject) => {
				axios.post(baseApiUrl + 'auth/login.php', credentials)
					.then( response =>{

						let obj = response.data.data
	

						if( obj.user ) {
							localStorage.setItem('access_token', obj.token);
							context.commit('statusChange', response.data.message);
							context.commit('retrieveToken', obj.token);
							context.commit('setUser', obj.user);
							resolve( response );
							// console.log(response)
						} else {
							console.log('Error: ' + response.data.message );
							context.commit('statusChange', response.data.message);
						}
						
					})
					.catch( error => {
						// console.log( error );
						reject( error );
					})
			})
		},
		destroyToken(context) {
			if( context.getters.isUserLoggedIn) {
				return new Promise((resolve, reject) => {
					let uid = context.state.user;
				    const logOutData = new FormData();

		            logOutData.append('userid', uid);

					axios.post(baseApiUrl + 'auth/logout.php', logOutData)
						.then( response =>{

							localStorage.removeItem('access_token');
							context.commit('statusChange', response.data.message);
							context.commit('destroyToken');
							resolve( response );
							
						})
						.catch( error => {
							// console.log( error );
							localStorage.removeItem('access_token');
							context.commit('statusChange', 'An error has occurred whilst logging you out.');
							context.commit('destroyToken');
							reject( error );
						})
				})
			}
		},
		registerUser(context, data) {
			return new Promise((resolve, reject) => {
				axios.post(baseApiUrl + 'auth/register.php', data)
					.then( response =>{

						let obj = response.data.data
			

						if( response.status == 200 ) {
							context.commit('statusChange', response.data.message);
							resolve( response );
							// console.log(response)
						} else {
							console.log('Error: ' + response.data.message );
							context.commit('statusChange', response.data.message);
						}
						
					})
					.catch( error => {
						// console.log( error.response );
						context.commit('statusChange', error.response.data.message)
						reject( error );
					})
			})
		},
		getSheetData(context){
			return new Promise((resolve, reject) => {
				axios.post(baseApiUrl + 'google/googlesheets.php')
					.then( response =>{

						let obj = response.data.data
						
						// console.obj;

						if( obj.values ) {
							// context.commit('statusChange', response.data.message);
							context.commit('setSheetData', obj.values);
							// context.dispatch('setNewAmount');
							resolve( obj.values );
							// console.log(response)
						} else {
							// console.log('Error: ' + response.data.message );
							context.commit('statusChange', 'Could not update table because: <br/>' + response.data.message);
							reject(response.data.message)
						}
						
					})
					.catch( error => {
						// console.log( error );
						context.commit('statusChange', 'Could not update table because: <br/> ' + error);
						reject( error );
					})
			})
		},
		setNewAmount(context,  payload){
		    
		    // console.log(payload)
		    //TODO: figure out why FormData was not working...
		    let newPayload = new FormData();
		    newPayload.append('payload',payload);

		    // console.log(newPayload)

            // if( !uindex ) {
            // 	console.log('No index passed');
            // 	return;
            // }

            // if( !amount ) {
            // 	console.log('No amount passed');
            // 	return;
            // }

			return new Promise((resolve, reject) => {
				axios.post(baseApiUrl + 'google/updateSheet.php', payload)
					.then( response =>{

						let obj = response.data.data
						
						console.obj;

						if( obj ) {
							context.commit('statusChange', response.data.message);
							// context.commit('setSheetData', obj.values);
							// context.dispatch('setNewColumn');
							resolve( obj );
							// console.log(response)
						} else {
							// console.log('Error: ' + response.data.message );
							context.commit('statusChange', response.data.message);
							reject(response.data.message);
						}
						
					})
					.catch( error => {
						// console.log( error );
						reject( error );
					})
			})
		},
		registerUser(context, payload){
			var payload = payload;
			return new Promise((resolve, reject) => {
				axios.post(baseApiUrl + 'google/updateSheet.php', payload)
					.then( response =>{

						let obj = response.data.data
						
						console.obj;

						if( obj.values ) {
							context.commit('statusChange', response.data.message);
							// context.dispatch('setNewAmount');
							resolve( obj.values );
							// console.log(response)
						} else {
							// console.log('Error: ' + response.data.message );
							context.commit('statusChange', response.data.message);
							reject( response.data.message);
						}
						
					})
					.catch( error => {
						// console.log( error );
						reject( error );
					})
			})
		},
		updateNotes(context,  payload){

			return new Promise((resolve, reject) => {
				axios.post(baseApiUrl + 'google/updateSheet.php', payload)
					.then( response =>{

						let obj = response.data.data
						
						console.obj;

						if( obj ) {
							context.commit('statusChange', response.data.message);
							// context.commit('setSheetData', obj.values);
							// context.dispatch('setNewColumn');
							resolve( obj );
							// console.log(response)
						} else {
							console.log('Error: ' + response.data.message );
							context.commit('statusChange', response.data.message);
						}
						
					})
					.catch( error => {
						// console.log( error );
						reject( error );
					})
			})
		},
	},
	getters: {
		isUserLoggedIn (state) {
		    return state.token !== null
		}
	}
})