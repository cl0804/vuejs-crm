import axios from 'axios';
import moment from 'moment-timezone';
import Vue from 'vue';
import VueRouter from 'vue-router';
import VueResource from 'vue-resource';
// import VueSession from 'vue-session';

import App from './components/app.vue';
import Login from './components/login.vue';
import routes from './routes/routes';
import store from './store';
import './style.scss';


Vue.use(VueRouter);
Vue.use(VueResource);
// Vue.use(VueSession)
const router = new VueRouter({ routes });



moment.tz.setDefault('UTC');
Object.defineProperty(Vue.prototype, '$moment', { get() { return this.$root.moment } });



router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresVisitor)) {
    // this route requires visitors, check if logged in
    // if yes, redirect to homepage
    // if not, redirect to login page.
    if (store.getters.isUserLoggedIn ) {
      next({
        path: '/',
      })
    } else {
      next()
    }
  } else {
    next() // make sure to always call next()!
  }
})

new Vue({
  el: '#app',
  data: {
    moment
  },
  components: {
  	App,
  	Login
  },
  store,
  router
});
