<?php 
	header( 'Content-Type: application/json' );
	header( 'Access-Control-Allow-Origin: *' );

	require_once '../../init.php';
	require_once '../../apiTypes.php';

	if( isset( $_POST['Username']) && isset( $_POST['Password']) ) {
		if( !empty( $_POST['Username']) && !empty( $_POST['Password']) )
		{	
			//Need to clean these up before query
			$username = SanitiseInputStr($_POST['Username']);
			$password = SanitiseInputStr($_POST['Password']);
			$resp = new ApiResponse();

			if( registerNewUser($username, $password, $db) ) {
				$resp->message = 'User successfully created';
				echo json_encode($resp);
				http_response_code(200);
			} else {
				$e = outputErrors( $ErrorString );
				$resp->data['serverMsg'] = $e;
				$resp->message = 'Failed to create new user because of the following reasons: ' . $e;
				echo json_encode($resp);
				http_response_code(400);
			}
		}
	} else {
		outputErrors( $ErrorString );
		http_response_code(400);
	}
?>