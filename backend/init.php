<?php

session_start();

$ErrorString = array();

require_once __DIR__ . '/declarations.php';
require_once __DIR__ . '/database/dbconnect.php';
require_once __DIR__ . '/functions/functions.php';
require_once __DIR__ . '/functions/user_functions.php';