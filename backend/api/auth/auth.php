<?php 
header( 'Content-Type: application/json' );
header( 'Access-Control-Allow-Origin: *' );

require_once '../init.php';

// print_r($_POST);
$token = $_POST['token'];

class authResponse {
	public $data = '';
	public $state = '';
}

$dbToken = isLoggedIn($token, $db);

// echo $dbToken . '<br>';
// echo $token;
if( $dbToken ) {
	echo '';
	$resp = new authResponse();
	$resp->data = 'Token found';
	$resp->state = 'Pass';
	echo json_encode($resp);
} else {
	$resp = new authResponse();
	$resp->data = 'Token not found';
	$resp->state = 'Fail';
	echo json_encode($resp);
}
// if( !empty( $token ) ) {
// 	if( isLoggedIn($token, $db) ) {
// 		$resp = new authResponse();
// 		$resp->data = 'Auth Complete';
// 		$resp->state = true;
// 		$resp->token = $token;
// 		echo json_encode($resp);
// 	}
// }

?>