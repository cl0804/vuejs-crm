<?php 
	header( 'Content-Type: application/json' );
	header( 'Access-Control-Allow-Origin: *' );

	require_once '../../init.php';
	require_once '../../apiTypes.php';
	


	$permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	$token = generate_string($permitted_chars, 64);

	if( isset( $_POST['Username']) && isset( $_POST['Password']) ) {
		if( !empty( $_POST['Username']) && !empty( $_POST['Password']) )
		{	
			//Need to clean these up before query
			$username = SanitiseInputStr($_POST['Username']);
			$password = SanitiseInputStr($_POST['Password']);

			//Run Login function
			if( Login( $username, $password, $db, $token) ) {

				//Checks for is allowed flag in db
				$isAllowed = isUserAllowed( $_SESSION['UserId'], $db );
				$resp = new ApiResponse();
				if( $isAllowed ) {
					$resp->message = 'Login for you!';
					$resp->data['user'] = $_SESSION["UserId"];
					$resp->data['token'] = $token;
					http_response_code(200);
				} else {
					$resp->message = 'You\'re account is currently deactivated, please speak to an admin';
					http_response_code(403);
				}
				echo json_encode($resp);
				return;
			} else {
				$resp = new ApiResponse();
				$resp->message = 'Username or password incorrect';
				http_response_code(400);
				echo json_encode($resp);
			}
		} else {
			$resp = new ApiResponse();
			$resp->message = 'Both fields required';
			http_response_code(400);
			echo json_encode($resp);
			return false;
		}
	} else {
		$resp = new ApiResponse();
		$resp->message = 'No login for you!';
		http_response_code(400);
		echo json_encode($resp);
	}
?>