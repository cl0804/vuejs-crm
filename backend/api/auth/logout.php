<?php 
	header( 'Content-Type: application/json' );
	header( 'Access-Control-Allow-Origin: *' );

	require_once '../../init.php';
	require_once '../../apiTypes.php';

	// print_r($_POST);

	$userid = $_POST['userid'];

	if( LogOut($userid,$db) ) {
		$resp = new ApiResponse();
		$resp->message = 'User Successfully Logged Out';
		echo json_encode($resp);
	} else {
		$resp = new ApiResponse();
		$resp->message = 'Error Logging user out.';
		echo json_encode($resp);
	}

