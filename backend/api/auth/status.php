<?php 

class sessionResponse {
	public $user = null;
	public $auth = false;
}

if( isset($_SESSION['UserId']) ) {
	$resp = new LoginResponse();
	$resp->user = $_SESSION["UserId"];
	$resp->auth = true;
	echo json_encode($resp);
}