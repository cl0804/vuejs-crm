<?php
header( 'Content-Type: application/json' );
header( 'Access-Control-Allow-Origin: *' );

require_once '../../init.php';
require_once '../../apiTypes.php';

$resp = new ApiResponse();

// Autoload Composer.
require_once 'A:\PHP Stuff\Xamp\htdocs\arc-crm\backend\vendor\autoload.php';

//TODO: see if this can be cleaned up like login.php
$_POST = json_decode(file_get_contents("php://input"),true);

if(isset($_POST['amount']) && isset($_POST['index']) ) {

$userCreds = __DIR__ . '/creds.json';
$client = getClient($userCreds);
$service = new Google_Service_Sheets($client);

$data = [];

// The first row contains the column titles, so lets start pulling data from row 2
$currentRow = 2;

$spreadsheetId = '1WgcQLmnqqqCJqqpNGe-GmmUjO7jRCt_rw-TaaYMS0NY';  
$range = 'Form responses 1'; 
$rows = $service->spreadsheets_values->get($spreadsheetId, $range, ['majorDimension' => 'ROWS']);


	

	$amount = SanitiseInputStr($_POST['amount']);
	$index = SanitiseInputStr($_POST['index']);
	$type = SanitiseInputStr($_POST['type']);

	if (isset($rows['values'])) {
	
		if( !empty($rows['values'][$index][25]) ) {
			$currentPaid = $rows[$index][25];
			$newAmount = $currentPaid + $amount;
		} else {
			$newAmount = $amount;
		}

		$row = $index + 1;
		
		// $updateRange = 'Z'.$row;
  //       $updateBody = new \Google_Service_Sheets_ValueRange([
  //           'range' => $updateRange,
  //           'majorDimension' => 'ROWS',
  //           'values' => ['values' => $newAmount],
  //       ]);
  //       $service->spreadsheets_values->update(
  //           $spreadsheetId,
  //           $updateRange,
  //           $updateBody,
  //           ['valueInputOption' => 'USER_ENTERED']
  //       );

		$lineOneValues = [
		    [
		        $newAmount,
		        $type
	        ]
		    // Additional rows ...
		];

		$lineOneData = [];
		$lineOneData[] = new Google_Service_Sheets_ValueRange([
		    'range' => 'Z'.$row.':AA'.$row,
		    'values' => $lineOneValues
		]);

		$requestBody = new Google_Service_Sheets_BatchUpdateValuesRequest([
		  'valueInputOption' => 'USER_ENTERED',
		  'data' => $lineOneData
		]);

		$lineOneResponse = $service->spreadsheets_values->batchUpdate($spreadsheetId, $requestBody);

	  // $resp->data['values'] = $rows['values'][$index];
	  $resp->message = 'Balance updated';
	  echo json_encode($resp);
	  http_response_code(200);
	}
} else if( isset($_POST['registerStamp']) && isset($_POST['index']) ) {

	$userCreds = __DIR__ . '/creds.json';
	$client = getClient($userCreds);
	$service = new Google_Service_Sheets($client);

	$data = [];

	// The first row contains the column titles, so lets start pulling data from row 2
	$currentRow = 2;

	$spreadsheetId = '1WgcQLmnqqqCJqqpNGe-GmmUjO7jRCt_rw-TaaYMS0NY';  
	$range = 'Form responses 1'; 
	$rows = $service->spreadsheets_values->get($spreadsheetId, $range, ['majorDimension' => 'ROWS']);

	$registerStamp = SanitiseInputStr($_POST['registerStamp']);
	$index = SanitiseInputStr($_POST['index']);

	if (isset($rows['values'])) {
	
		$row = $index + 1;
		
		$updateRange = 'AB'.$row;
        $updateBody = new \Google_Service_Sheets_ValueRange([
            'range' => $updateRange,
            'majorDimension' => 'ROWS',
            'values' => ['values' => $registerStamp],
        ]);
        $service->spreadsheets_values->update(
            $spreadsheetId,
            $updateRange,
            $updateBody,
            ['valueInputOption' => 'USER_ENTERED']
        );

	  $resp->data['values'] = $registerStamp;
	  $resp->message = 'User registered';
	  echo json_encode($resp);
	  http_response_code(200);
	}
} else if(isset($_POST['notes']) && isset($_POST['index'])) {

	$userCreds = __DIR__ . '/creds.json';
	$client = getClient($userCreds);
	$service = new Google_Service_Sheets($client);

	$data = [];

	// The first row contains the column titles, so lets start pulling data from row 2
	$currentRow = 2;

	$spreadsheetId = '1WgcQLmnqqqCJqqpNGe-GmmUjO7jRCt_rw-TaaYMS0NY';  
	$range = 'Form responses 1'; 
	$rows = $service->spreadsheets_values->get($spreadsheetId, $range, ['majorDimension' => 'ROWS']);

	$notes = htmlentities($_POST['notes']);
	$index = SanitiseInputStr($_POST['index']);

	if (isset($rows['values'])) {

		$row = $index + 1;
		
		$updateRange = 'AC'.$row;
        $updateBody = new \Google_Service_Sheets_ValueRange([
            'range' => $updateRange,
            'majorDimension' => 'ROWS',
            'values' => ['values' => $notes],
        ]);
        $service->spreadsheets_values->update(
            $spreadsheetId,
            $updateRange,
            $updateBody,
            ['valueInputOption' => 'USER_ENTERED']
        );

	  // $resp->data['values'] = $rows['values'][$index];
	  $resp->message = 'Notes updated';
	  echo json_encode($resp);
	  http_response_code(200);
	}
} else {
	$resp->data['post'] = $_POST;
	$resp->message = 'Nope!';
	echo json_encode($resp);
		  // http_response_code(404);
}



// $response = $service->spreadsheets_values->get($spreadsheetId, $range);
// $values = $response->getValues();



// if( $values ) {
//   $resp->data['values'] = $values;
//   $resp->message = 'Data received';
//   echo json_encode($resp);
//   http_response_code(200);
// } else {
//   $resp->message = 'Sad face emoji';
//   echo json_encode($resp);
//   http_response_code(404);
// }


function getClient($userCreds) {
  $client = new Google_Client();
  $client->setApplicationName('Google Sheets and PHP');
  $client->setScopes(Google_Service_Sheets::SPREADSHEETS);
  $client->setAuthConfig( $userCreds );
  $client->setAccessType('offline');
  return $client;
}


?>