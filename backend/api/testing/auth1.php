<?php 

// header( 'Content-Type: application/json' );
// header( 'Access-Control-Allow-Origin: *' );

require_once '../../init.php';
require_once '../../apiTypes.php';

if( isset( $_POST ) && !(empty($_POST)) ) {
	
	$resp = new ApiResponse(); 

	$resp->data['post_data'] = var_dump($_POST);
	
	//Validate all post variables here

	if( !isset($_POST['token']) ) {
		http_response_code(404);
		$resp->message = 'Token missing from post';
		echo json_encode($resp);
		return;
	}

	//SANITSE!!!!
	$token = $_POST['token'];

	if( !IsLoggedIn( $token, $db) ) {
		http_response_code(403);
		$resp->message = 'No user for token';
		echo json_encode($resp);
		return;
	}

	//DB lookups happen here
	$resp->data['response'] = 'User is authorized';
	echo json_encode($resp);
	return;
} 

if( isset( $_GET ) && !(empty($_GET)) ) {
	
	$resp = new ApiResponse(); 

	$resp->data['get_data'] = var_dump($_GET);
	
	//Validate all post variables here

	if( !isset($_GET['token']) ) {
		http_response_code(404);
		$resp->message = 'Token missing from get';
		echo json_encode($resp);
		return;
	}

	//SANITSE!!!!
	$token = $_GET['token'];

	if( !IsLoggedIn( $token, $db) ) {
		http_response_code(403);
		$resp->message = 'No user for token';
		echo json_encode($resp);
		return;
	}

	//DB lookups happen here such as data reading
	$resp->data['response'] = 'User is authorized';
	echo json_encode($resp);
	return;
} 
 
http_response_code(404);

?>